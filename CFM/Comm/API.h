//
//  API.h
//  HSE
//
//  Created by Selab on 4/24/17.
//  Copyright © 2017 Selab. All rights reserved.
//

#ifndef API_h
#define API_h

//#define BASE_URL @"http://10.1.3.32:8888/"
#define BASE_URL @"http://106.14.190.177:8082/cfm"

#define Api(api) [NSString stringWithFormat:@"%@%@", BASE_URL, api]

#define login @"/account/login"

#define userRegister @"/account/register"

#define modifyPwd @"/account/modifyPwd"

#define warnInfors @"/warning/infors"

#define warnHistory @"/warning/history"

#define recordList @"/child/recordList"

#define leaveChild @"/child/leave"


#endif /* API_h */
