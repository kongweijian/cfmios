//
//  UserInfo.m
//  HSE
//
//  Created by Selab on 4/19/17.
//  Copyright © 2017 Selab. All rights reserved.
//

#import "UserInfo.h"

@implementation UserInfo

static UserInfo *data = nil;

//单例类使用此方法，创建单例对象

+ (instancetype)setData{

    if (data == nil) {
        
        data = [[UserInfo alloc] init];
    }
    return data;
    
}

@end