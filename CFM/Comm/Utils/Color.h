//
//  Color.h
//  HSE
//
//  Created by Selab on 4/22/17.
//  Copyright © 2017 Selab. All rights reserved.
//

#ifndef Color_h
#define Color_h

#define   TITLE_COLOR [UIColor colorWithHexString:@"3ca0ec"]
#define   BG_COLOR [UIColor colorWithHexString:@"f5f5f5"]
#define   TEXT_COLOR [UIColor colorWithHexString:@"8B8B83"]
#define   LINE_COLOR [UIColor colorWithHexString:@"8B8B83"]
#define   BLUE_COLOR [UIColor colorWithHexString:@"68D5FC"]
#define   TABTEXT_COLOR [UIColor colorWithHexString:@"453aa9"]
#define   MAIN_COLOR [UIColor colorWithHexString:@"3ca0ec"]

#endif /* Color_h */
