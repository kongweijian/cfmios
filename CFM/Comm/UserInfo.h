//
//  UserInfo.h
//  HSE
//
//  Created by Selab on 4/19/17.
//  Copyright © 2017 Selab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserInfo : NSObject
+ (instancetype)setData;

@property(nonatomic)NSInteger userId;
@property (nonatomic,strong)NSString *hospital;
@property (nonatomic,strong)NSString *userName;

@end
