//
//  MonitorVC.h
//  CFM
//
//  Created by Selab on 01/02/2018.
//  Copyright © 2018 Selab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MonitorVC: BaseVC

@property (nonatomic,retain) UITableView *tableView;
@property (nonatomic,retain) UILabel *name;
@property (nonatomic) NSInteger index;
@property (nonatomic,retain) NSMutableArray *arr;
@property (nonatomic,retain) NSMutableArray *resultArr;
@property (nonatomic,retain) NSMutableArray *resultIdArr;
@property (nonatomic,copy) NSDictionary *userInfo;

@end
