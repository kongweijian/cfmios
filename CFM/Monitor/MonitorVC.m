//
//  ViewController.m
//  miSearch
//
//  Created by miicaa_ios on 16/8/3.
//  Copyright (c) 2016年 xuxuezheng. All rights reserved.
//
#import "MonitorVC.h"
#import "MonitorCell.h"
#import <AVFoundation/AVFoundation.h>
#import "WarnHistoryVC.h"

@interface MonitorVC ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,EMChatManagerDelegate,UITabBarDelegate>
@end

@implementation MonitorVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"监测";
    self.backBtn.hidden = YES;
    [self.navigationItem setHidesBackButton:YES];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,WIN_WIDTH, WIN_HEIGHT-80) style:UITableViewStylePlain];
    _tableView.delegate  =self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [[UIView alloc] init];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UILongPressGestureRecognizer *longGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGesture:)];
    [self.tableView addGestureRecognizer:longGesture];
    [self.view addSubview:_tableView];
    _arr = [[NSMutableArray alloc] init];
    _resultArr = [[NSMutableArray alloc] init];
    _resultIdArr =[[NSMutableArray alloc] init];
    
    [self getWarnInforsList];
    
    //注册消息回调
    [[EMClient sharedClient].chatManager addDelegate:self delegateQueue:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(registerCompletion:)
                                                 name:@"RegisterCompletionNotification"
                                               object:nil];
}

- (UIImage *)createImageWithColor: (UIColor *) color
{
    CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MonitorCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchCell"];
    if (!cell) {
        cell = [[MonitorCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SearchCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (_arr.count>0) {
        NSMutableDictionary *temp = _arr[indexPath.row];
        cell.name.text = [temp objectForKey:@"childName"];
        cell.bed.text = [temp objectForKey:@"bed"];
        cell.time.text = [NSString stringWithFormat:@"%@%@", @"报警时间：", [temp objectForKey:@"dateTime"]];
        NSString *state = [temp objectForKey:@"deviceState"];
        NSLog(@"searchText --%@",state);
        if ([state containsString:@"连接正常"]) {
            UIImage *image = [UIImage imageNamed:@"normal"];
            cell.img.image = image;
        }else if ([state containsString:@"已脱落"]) {
            UIImage *image = [UIImage imageNamed:@"yichangs"];
            cell.img.image = image;
        }else{
            UIImage *image = [UIImage imageNamed:@"duan"];
            cell.img.image = image;
        }
    } else {
        NSLog(@"searchText --%@",@"空");
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableDictionary *temp = _arr[indexPath.row];
    WarnHistoryVC *vc = [[WarnHistoryVC alloc] init];
    vc.childId = [[temp objectForKey:@"childId"] integerValue];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
}

-(void) getWarnInforsList{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
   [params setValue:[UserInfo setData].hospital forKey:@"hospital"];
    [[NetworkTool sharedTool] requestWithURLString:Api(warnInfors) parameters:params method:@"GET" callBack:^(id responseObject) {
        NSLog(@"%@",responseObject);
        _arr = [responseObject objectForKey:@"warnInforsList"];
        [self.tableView reloadData];
    }];
}

- (void)longPressGesture:(UILongPressGestureRecognizer *)longGesture
{
    CGPoint location = [longGesture locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    _index=indexPath.row;
    if (longGesture.state == UIGestureRecognizerStateEnded){
        return;
    }else if (longGesture.state == UIGestureRecognizerStateBegan){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"是否确定不再关注改病人？若不再关注则收不到他的报警信息" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alert show];
        
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex) {
         NSLog(@"%@",@"11111");
        NSMutableDictionary *temp = _arr[_index];
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setValue:@([[temp objectForKey:@"childId"] integerValue]) forKey:@"childId"];
        [[NetworkTool sharedTool] requestWithURLString:Api(leaveChild) parameters:params method:@"GET" callBack:^(id responseObject) {
            NSLog(@"%@",responseObject);
            [self getWarnInforsList];
        }];
    } else {
          NSLog(@"%@",@"222");
    }
}

- (void)viewDidAppear:(BOOL)animated{
    
    self.tabBarController.tabBar.hidden = NO;
    [self getWarnInforsList];
    [super viewDidAppear:animated];
}

//视图将要消失
- (void)viewWillDisappear:(BOOL)animated {
    NSLog(@"%s", __FUNCTION__);
    [super viewWillDisappear:animated];
}

//视图已经消失
- (void)viewDidDisappear:(BOOL)animated {
    NSLog(@"%s", __FUNCTION__);
    [super viewDidDisappear:animated];
}

- (void)messagesDidReceive:(NSArray *)aMessages{
    [self getWarnInforsList];
    for (EMMessage *message in aMessages) {
        EMMessageBody *msgBody = message.body;
        if(msgBody.type==EMMessageBodyTypeText){
            // 收到的文字消息
            EMTextMessageBody *textBody = (EMTextMessageBody *)msgBody;
            NSString *txt = textBody.text;
            NSLog(@"txttxttxt---%@", txt);
            
            AVSpeechSynthesisVoice *voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"zh-CN"];
            AVSpeechSynthesizer *synthesizer = [[AVSpeechSynthesizer alloc] init];
            NSError *error = NULL;
            AVAudioSession *session = [AVAudioSession sharedInstance];
            [session setCategory:AVAudioSessionCategoryPlayback error:&error];
            AVSpeechUtterance *utterance = [AVSpeechUtterance speechUtteranceWithString:txt];
            utterance.volume = 0.12;
            utterance.voice = voice;
            utterance.rate = 0.52;
            [synthesizer speakUtterance:utterance];
        }
    }
}

- (void)cmdMessagesDidReceive:(NSArray *)aCmdMessages{
    
    [[[ZFToast alloc] init] popUpToastWithMessage:@"cmdMessagesDi"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)registerCompletion:(NSNotification*)notification {
    NSDictionary *theData = [notification userInfo];
    NSString *username = [theData objectForKey:@"username"];
    
    NSLog(@"username = %@",username);
    //注册消息回调
    [[EMClient sharedClient].chatManager addDelegate:self delegateQueue:nil];
   // AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}


@end



