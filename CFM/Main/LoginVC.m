
//
//  RootController.m
//  DJRegisterViewDemo
//
//  Created by asios on 15/8/15.
//  Copyright (c) 2015年 梁大红. All rights reserved.
//

#import "LoginVC.h"
#import "MainVC.h"
#import "RegisterVC.h"

@interface LoginVC ()
//@property(nonatomic,strong)AVAudioPlayer *movePlayer ;
@end

NSString *imgUrla;
UITextField *accText;
UITextField *passText;
UIImageView *accImage;
UIImageView *passImage;
UIButton *loginBtn,*regBtn,*pwdBtn,*findPwdBtn;

@implementation LoginVC

static NSInteger seq = 0;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden=YES;
    self.backBtn.enabled = NO;
    UIImage *bgImg = [UIImage imageNamed:@"loginbg"];
    
    CGRect imageRect = CGRectMake(0.0, 20.0, WIN_WIDTH, WIN_HEIGHT);
    [bgImg drawInRect:imageRect];
    UIImageView *bgView = [[UIImageView alloc]initWithImage:bgImg];
    
    bgView.frame = self.view.bounds;
    
    [self.view addSubview:bgView];
    // 账户
    accText = [[UITextField alloc] initWithFrame:CGRectMake(40, WIN_HEIGHT/2-80, WIN_WIDTH-80, 50)];
    accText.clearButtonMode = UITextFieldViewModeAlways;
    accText.placeholder = @" 账 户";
    accText.tag = 1;
    accText.backgroundColor = [UIColor whiteColor];
    accText.layer.borderColor = MAIN_COLOR.CGColor;
    accText.layer.masksToBounds = YES;
    accText.layer.cornerRadius = 10;
    accText.layer.borderWidth= 1.0f;
    [self.view addSubview:accText];
    
    //icon
    UIImageView *accIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    accIcon.image = [UIImage imageNamed:@"user"];
    accText.leftView = accIcon;
    accText.leftViewMode = UITextFieldViewModeAlways;
    // 线
    accImage = [[UIImageView alloc] initWithFrame:CGRectMake(20, 150, WIN_WIDTH-40, 2)];
    accImage.image = [UIImage imageNamed:@"textfield_activated_holo_light.9.png"];
    // [self.view addSubview:accImage];
    
    // 密码
    passText = [[UITextField alloc] initWithFrame:CGRectMake(40, WIN_HEIGHT/2-10, WIN_WIDTH-80, 50)];
    [passText setSecureTextEntry:YES];
    passText.clearButtonMode = UITextFieldViewModeAlways;
    passText.backgroundColor = [UIColor whiteColor];
    passText.layer.borderColor = MAIN_COLOR.CGColor;
    passText.layer.masksToBounds = YES;
    passText.layer.cornerRadius = 10;
    passText.layer.borderWidth= 1.0f;
    [self.view addSubview:passText];
    
    //icon
    UIImageView *passIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    passIcon.image = [UIImage imageNamed:@"password"];
    passText.leftView = passIcon;
    passText.leftViewMode = UITextFieldViewModeAlways;
    passText.placeholder = @" 密 码";
    // 线
    passImage = [[UIImageView alloc] initWithFrame:CGRectMake(20, 200, WIN_WIDTH-40, 2)];
    passImage.image = [UIImage imageNamed:@"textfield_default_holo_light.9.png"];
    passText.tag = 0;
   //[self.view addSubview:passImage];
    
    // 登录
    loginBtn = [UIButton buttonWithType:0];
    loginBtn.frame = CGRectMake(40, WIN_HEIGHT/2+60, WIN_WIDTH-80, 50);
    [loginBtn setTitle:@"登 录" forState:0];
    loginBtn.backgroundColor = MAIN_COLOR;
    [self.view addSubview:loginBtn];
    loginBtn.clipsToBounds = YES;
    loginBtn.layer.cornerRadius = 8.0f;
    [loginBtn addTarget:self action:@selector(loginBtnClick)forControlEvents:UIControlEventTouchUpInside];
    
    regBtn = [UIButton buttonWithType:0];
    regBtn.frame = CGRectMake(35, WIN_HEIGHT/2+125, 60, 40);
    [regBtn setTitle:@"注 册" forState:0];
    [regBtn setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
    regBtn.backgroundColor = [UIColor clearColor];
   // [self.view addSubview:regBtn];
    regBtn.clipsToBounds = YES;
    regBtn.layer.cornerRadius = 8.0f;
    [regBtn addTarget:self action:@selector(registerClick)forControlEvents:UIControlEventTouchUpInside];
    
    findPwdBtn = [UIButton buttonWithType:0];
    findPwdBtn.frame = CGRectMake(WIN_WIDTH-110, 380, 100, 40);
    [findPwdBtn setTitle:@"找回密码" forState:0];
    [findPwdBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    findPwdBtn.backgroundColor = [UIColor clearColor];
    //[self.view addSubview:findPwdBtn];
    findPwdBtn.clipsToBounds = YES;
    findPwdBtn.layer.cornerRadius = 8.0f;
    [findPwdBtn addTarget:self action:@selector(findPwdClick)forControlEvents:UIControlEventTouchUpInside];
    
}

- (NSInteger)seq {
    return ++ seq;
}

-(void)loginBtnClick{
    if ([accText.text length] == 0) {
        [[[ZFToast alloc] init] popUpToastWithMessage:@"账号不能为空"];
    } else if([passText.text length] == 0){
        [[[ZFToast alloc] init] popUpToastWithMessage:@"密码不能为空"];
    }else{
        [self loginAction];
    }
}

-(void) registerClick{
    
    RegisterVC *vc = [[RegisterVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
    self.navigationController.navigationBar.hidden=YES;
}

-(void)findPwdClick{
//
//    UpdatePwdVC *vc = [[UpdatePwdVC alloc] init];
//    [self.navigationController pushViewController:vc animated:YES];
}


-(void)loginAction{
     NSLog(@"params--");
    [[EMClient sharedClient] loginWithUsername:accText.text password:[NSString stringWithFormat:@"%@%@", [accText.text substringToIndex:1], @"1"] completion:^(NSString *aUsername, EMError *aError) {
        if (!aError) {
        } else {
            NSLog(@"登录失败");
        }
    }];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:accText.text forKey:@"userName"];
    [params setValue:passText.text forKey:@"passWord"];
    //[params setValue:[UserInfo setData].deviceToken forKey:@"deviceToken"];
    [[NetworkTool sharedTool] requestWithURLString:Api(login) parameters:params method:@"POST" callBack:^(id responseObject) {
        NSLog(@"%@",responseObject);
        if([[responseObject objectForKey:@"state"] isEqual:@"success"]){
            NSString *userId = [responseObject objectForKey:@"userId"];
            [[UserInfo setData] setUserId:[userId integerValue]];
            [[UserInfo setData] setHospital:[responseObject objectForKey:@"hospital"]];
            [[UserInfo setData] setUserName:accText.text];
             MainVC *vc = [[MainVC alloc] init];
            [self presentViewController:vc animated:YES completion:nil];
        }else {
            [[[ZFToast alloc] init] popUpToastWithMessage:@"登录失败！"];
        }
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - touch
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UIView *view in [self.view subviews]) {
        [view resignFirstResponder];
    }
}

- (void)license:(NSString *)license {
    accText.text = license;
}

@end

