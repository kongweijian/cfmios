//
//  MainVC.m
//  HSE
//
//  Created by Selab on 4/15/17.
//  Copyright © 2017 Selab. All rights reserved.
//

#import "MainVC.h"
#import "MonitorVC.h"
#import "SetVC.h"
#import "RecordVC.h"

@interface MainVC ()

@end

@implementation MainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    //self.backBtn.hidden = YES;
    //  self.navigationController.navigationBar.barTintColor = [UIColor redColor];
    [self checkController];
}

- (void)checkController{
    
    MonitorVC *vc1 = [[MonitorVC alloc] init];
    UINavigationController *nv_vc1 = [self setUpOneChildViewController:vc1 image:@"jc_normal" imagePre:@"jc_select" title:@"监测"];
    
    RecordVC *vc2=[[RecordVC alloc] init];
    UINavigationController *nv_vc2 = [self setUpOneChildViewController:vc2 image:@"ls_normal" imagePre:@"ls_select" title:@"记录"];
     
    SetVC *vc3=[[SetVC alloc] init];
    UINavigationController *nv_vc3 = [self setUpOneChildViewController:vc3 image:@"set_normal" imagePre:@"set_select" title:@"设置"];
    
    tab=[[UITabBarController alloc]init];
    tab.viewControllers=[NSArray arrayWithObjects:nv_vc1,nv_vc2,nv_vc3, nil];
    [self.view addSubview:tab.view];
    
}

- (UINavigationController*)setUpOneChildViewController:(UIViewController *)vc image:(NSString *)imageName imagePre:(NSString *)imageNamePre title:(NSString *)title{
    
    UINavigationController *nv=[[UINavigationController alloc]
                                initWithRootViewController:vc];
    // vc.navigationItem.title = title;
    vc.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [vc.navigationController.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName:FONT(18),
       NSForegroundColorAttributeName:[UIColor whiteColor]}];
    UIImage *homeImageSel = [UIImage imageNamed:imageNamePre];
    homeImageSel = [homeImageSel imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    nv.tabBarItem.title = title;
    [nv.tabBarItem setTitleTextAttributes:@{NSFontAttributeName:FONT(14),NSForegroundColorAttributeName:[UIColor grayColor]}forState:UIControlStateNormal];
    [nv.tabBarItem setTitleTextAttributes:@{NSFontAttributeName:FONT(14),NSForegroundColorAttributeName:BLUE_COLOR}forState:UIControlStateSelected];
    nv.tabBarItem.image = [UIImage imageNamed:imageName];
    nv.tabBarItem.selectedImage = homeImageSel;
    nv.navigationBar.barTintColor = BLUE_COLOR;
    return nv;
}


@end

