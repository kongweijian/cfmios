//
//  AppDelegate.m
//  CFM
//
//  Created by Selab on 31/01/2018.
//  Copyright © 2018 Selab. All rights reserved.
//
#import "AppDelegate.h"
#import "LoginVC.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

#define appKey  @"1180171117178892#cfm"

@interface AppDelegate ()<EMChatManagerDelegate>

//时间片标识begin
@property UIBackgroundTaskIdentifier newtaskID;
@property UIBackgroundTaskIdentifier oldtaskID;
//时间片标识end

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:[[LoginVC alloc] init]];
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    [self.window makeKeyAndVisible];
    self.window.rootViewController = nc;
    
    self.newtaskID = self.oldtaskID = UIBackgroundTaskInvalid;
    
    EMOptions *options = [EMOptions optionsWithAppkey:appKey];
    options.apnsCertName = @"cfm1w w w w";
    [[EMClient sharedClient] initializeSDKWithOptions:options];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}

- (void)applicationWillTerminate:(UIApplication *)application {
}

// APP进入后台
- (void)applicationDidEnterBackground:(UIApplication *)application{
    //时间片标识begin,APP进入后台
    self.newtaskID = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:NULL];
    if (self.newtaskID != UIBackgroundTaskInvalid && self.oldtaskID != UIBackgroundTaskInvalid){
        [[UIApplication sharedApplication] endBackgroundTask: self.oldtaskID];
    }
    self.oldtaskID = self.newtaskID;
    //时间片标识end
    // [[EMClient sharedClient] applicationDidEnterBackground:application];
     [[EMClient sharedClient].chatManager addDelegate:self delegateQueue:nil];
 
}

// APP将要从后台返回
- (void)applicationWillEnterForeground:(UIApplication *)application
{
    //时间片标识begin
    if (self.oldtaskID != UIBackgroundTaskInvalid) {
        [[UIApplication sharedApplication] endBackgroundTask: self.oldtaskID];
        self.oldtaskID = UIBackgroundTaskInvalid;
    }
    
    //时间片标识end
    [[EMClient sharedClient] applicationWillEnterForeground:application];
}

@end

