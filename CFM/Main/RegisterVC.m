//
//  RegisterVC.m
//  HSE
//
//  Created by Selab on 11/10/17.
//  Copyright © 2017 Selab. All rights reserved.
//

#import "RegisterVC.h"

@interface RegisterVC () <UITextFieldDelegate>

@end

UITextField *userText;
UITextField *pwdText;
UITextField *surePwdText;
UIButton *registerBtn;

@implementation RegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden=NO;
    self.navigationItem.title = @"注 册";
    self.view.backgroundColor = BG_COLOR;
    // 账户
    userText = [[UITextField alloc] initWithFrame:CGRectMake(20, 145, WIN_WIDTH-40, 42)];
    userText.clearButtonMode = UITextFieldViewModeAlways;
    userText.placeholder = @" 账 户";
    userText.tag = 1;
    userText.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor colorWithHexString:@"3ca0ec"]);
    userText.backgroundColor = [UIColor whiteColor];
    userText.layer.cornerRadius = 10;
    userText.layer.borderWidth= 1.0f;
    [self.view addSubview:userText];
    
    //icon
    CGRect iconRect = CGRectMake(5, 0, 25, 25);
    iconRect.origin.x -= 5;
    UIImageView *accIcon = [[UIImageView alloc] initWithFrame:iconRect];
    accIcon.image = [UIImage imageNamed:@"user"];
    userText.leftView = accIcon;
    userText.leftViewMode = UITextFieldViewModeAlways;
    
    // 密码
    pwdText = [[UITextField alloc] initWithFrame:CGRectMake(20, 203, WIN_WIDTH-40, 42)];
    [pwdText setSecureTextEntry:YES];
    pwdText.clearButtonMode = UITextFieldViewModeAlways;
    pwdText.backgroundColor = [UIColor whiteColor];
    pwdText.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor colorWithHexString:@"3ca0ec"]);
    pwdText.layer.masksToBounds = YES;
    pwdText.layer.cornerRadius = 10;
    pwdText.layer.borderWidth= 1.0f;
    [self.view addSubview:pwdText];
    //icon
    UIImageView *passIcon1 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 0, 25, 25)];
    passIcon1.image = [UIImage imageNamed:@"password"];
    pwdText.leftView = passIcon1;
    pwdText.leftViewMode = UITextFieldViewModeAlways;
    pwdText.placeholder = @" 密 码";
    
    // 密码
    surePwdText = [[UITextField alloc] initWithFrame:CGRectMake(20, 261, WIN_WIDTH-40, 42)];
    [surePwdText setSecureTextEntry:YES];
    surePwdText.clearButtonMode = UITextFieldViewModeAlways;
    surePwdText.backgroundColor = [UIColor whiteColor];
    surePwdText.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor colorWithHexString:@"3ca0ec"]);
    surePwdText.layer.masksToBounds = YES;
    surePwdText.layer.cornerRadius = 10;
    surePwdText.layer.borderWidth= 1.0f;
    [surePwdText setDelegate:self];
    [self.view addSubview:surePwdText];
    UIImageView *passIcon2 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 0, 25, 25)];
    passIcon2.image = [UIImage imageNamed:@"password"];
    surePwdText.leftView = passIcon2;
    surePwdText.leftViewMode = UITextFieldViewModeAlways;
    surePwdText.placeholder = @"验证密码";
    
    // 注册
    registerBtn = [UIButton buttonWithType:0];
    registerBtn.frame = CGRectMake(20, 330, WIN_WIDTH-40, 40);
    [registerBtn setTitle:@"确 定" forState:0];
    registerBtn.backgroundColor = [UIColor colorWithHexString:@"3ca0ec"];
    [self.view addSubview:registerBtn];
    registerBtn.clipsToBounds = YES;
    registerBtn.layer.cornerRadius = 8.0f;
    [registerBtn addTarget:self action:@selector(registerClick)forControlEvents:UIControlEventTouchUpInside];
}

// 失去第一响应者时调用
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    if(surePwdText.text!=pwdText.text){
      [[[ZFToast alloc] init] popUpToastWithMessage:@"两次密码不一致"];
    }
    return YES;
}

-(void)registerClick{
    
    if ([userText.text length] == 0) {
        [[[ZFToast alloc] init] popUpToastWithMessage:@"账号不能为空"];
    } else if([pwdText.text length] == 0){
        [[[ZFToast alloc] init] popUpToastWithMessage:@"密码不能为空"];
    }else if([surePwdText.text length] == 0){
        [[[ZFToast alloc] init] popUpToastWithMessage:@"验证密码不能为空"];
    }else{
        [self registerAction];
    }
}

-(void)registerAction{
  
    if(surePwdText.text!=pwdText.text){
        [[[ZFToast alloc] init] popUpToastWithMessage:@"两次密码不一致"];
        return;
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:userText.text forKey:@"userName"];
    [params setValue:pwdText.text forKey:@"passWord"];
    [params setValue:@"1" forKey:@"hospital"];
    [[NetworkTool sharedTool] requestWithURLString:Api(userRegister) parameters:params method:@"POST" callBack:^(id responseObject) {
        NSLog(@"%@",responseObject);
        if([[responseObject objectForKey:@"registerState"] isEqual:@"success"]){
           [[[ZFToast alloc] init] popUpToastWithMessage:@"注册成功！"];
            [[EMClient sharedClient] registerWithUsername:
             userText.text password:[NSString stringWithFormat:@"%@%@",[userText.text substringToIndex:1], @"1"]];
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [[[ZFToast alloc] init] popUpToastWithMessage:@"注册失败！"];
        }
    }];
}


@end
