//
//  BaseVC.m
//  HSE
//
//  Created by Selab on 4/26/17.
//  Copyright © 2017 Selab. All rights reserved.
//

#import "BaseVC.h"

@interface BaseVC ()

@end

@implementation BaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.barTintColor = MAIN_COLOR;
    self.view.backgroundColor = [UIColor  whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName:[UIFont systemFontOfSize:18],
       NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.tabBarController.tabBar.hidden = YES;
    self.navigationItem.leftBarButtonItem = nil;
    [self.navigationItem setHidesBackButton:YES];
    
    self.backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.backBtn.frame = CGRectMake(10, 5, 40, 30);
    UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 8,8, 18)];
    icon.image = [UIImage imageNamed:@"back_icon"];
    [self.backBtn addSubview:icon];
    
    [self.backBtn addTarget: self action: @selector(backClick) forControlEvents: UIControlEventTouchUpInside];
    UIBarButtonItem*back=[[UIBarButtonItem alloc]initWithCustomView:self.backBtn];
    self.navigationItem.leftBarButtonItem=back;
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    //设置成NO表示当前控件响应后会传播到其他控件上，默认为YES。
    tapGestureRecognizer.cancelsTouchesInView = NO;
    //将触摸事件添加到当前view
    [self.view addGestureRecognizer:tapGestureRecognizer];
    

}

-(void)keyboardHide:(UITapGestureRecognizer*)tap{
   
    [self.view endEditing:YES];
}

-(void)backClick{
    
    [self.navigationController popViewControllerAnimated:YES];
    self.tabBarController.tabBar.hidden = NO;
}

@end
