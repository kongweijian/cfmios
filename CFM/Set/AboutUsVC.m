//
//  DeviceBindVC.m
//  HSE
//
//  Created by Selab on 11/12/17.
//  Copyright © 2017 Selab. All rights reserved.
//

#import "AboutUsVC.h"

@interface AboutUsVC ()

@end

@implementation AboutUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = @"关于我们";
    
    UIImageView *accIcon = [[UIImageView alloc] initWithFrame:CGRectMake((WIN_WIDTH-80)/2, 200, 80, 80)];
    accIcon.image = [UIImage imageNamed:@"applogo"];
    [self.view addSubview:accIcon];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(20, 300, WIN_WIDTH-40, 100)];
    label.text = @"本产品用于实时查看医院导管连接状态!";
    label.textColor = TEXT_COLOR;
    //文字居中显示
    label.textAlignment = UITextAlignmentCenter;
    //自动折行设置
    label.lineBreakMode = UILineBreakModeWordWrap;
    label.numberOfLines = 0;
    [self.view addSubview:label];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
