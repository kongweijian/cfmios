//
//  UpdateVC.m
//  HSE
//
//  Created by Selab on 4/21/17.
//  Copyright © 2017 Selab. All rights reserved.
//

#import "ContactVC.h"

@interface ContactVC ()

@end

UITextField *textTf;
UIButton *sureBtn;

@implementation ContactVC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = @"联系方式";
    self.tabBarController.tabBar.hidden = NO;
    self.view.hidden = NO;
    [self initUI];
}

#pragma mark InitView
- (void)initUI{
    
    
    UIView *v4 = [self setView:@"check" title:@"电话" text:@"18817558938" top:161+30];
    [self.view addSubview:v4];
    
    UIView *v2 = [self setView:@"check" title:@"QQ" text:@"1214001865" top:161+30+41];
    [self.view addSubview:v2];
    
    UIView *v5 = [self setView:@"check" title:@"邮箱" text:@"1214001865@qq.com" top:161+30+82];
    [self.view addSubview:v5];
    
    
}

- (UIView*)setView:(NSString *)imageName title:(NSString *)title text:(NSString *)text top:(int)top{
    
    UIView *view = [[UIView alloc] initWithFrame:
                    CGRectMake(0, top, WIN_WIDTH, 41)];
    view.backgroundColor = [UIColor whiteColor];
    view.userInteractionEnabled = YES;
    UIImageView *leftImg = [[UIImageView alloc] initWithFrame:
                            CGRectMake(10, 5, 30, 30)];
    leftImg.image = [UIImage imageNamed:@""];
    UILabel *textLb = [[UILabel alloc]initWithFrame:
                       CGRectMake(WIN_WIDTH-125, 10, 125, 20)];
    textLb.text = text;
    textLb.textColor = TEXT_COLOR;
    textLb.font=FONT(12);
    [view addSubview:textLb];
    
    UILabel *nameLb = [[UILabel alloc]initWithFrame:
                       CGRectMake(30, 10, 100, 20)];
    nameLb.text = title;
    nameLb.textColor = TEXT_COLOR;
    nameLb.font=FONT(13);
    [view addSubview:nameLb];
    
    UIView *line = [[UIView alloc] initWithFrame:
                    CGRectMake(5, 40, WIN_WIDTH-10, 1)];
    line.backgroundColor = BG_COLOR;
    [view addSubview:line];
    return view;
}

@end
