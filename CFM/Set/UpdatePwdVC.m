//
//  UpdatePwdVC.m
//  HSE
//
//  Created by Selab on 4/21/17.
//  Copyright © 2017 Selab. All rights reserved.
//
#import "SetVC.h"
#import "UpdatePwdVC.h"

@interface UpdatePwdVC ()

@end

UITextField *pwdTf ;
UITextField *newPwdTf;
UITextField *inputPwdTf;

@implementation UpdatePwdVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = @"修改密码";
    UILabel *pwdLb=[[UILabel alloc]initWithFrame:CGRectMake(10, 105, 80, 25)];
    pwdLb.text = @"原始密码";
    pwdLb.font=[UIFont systemFontOfSize:13];
    [self.view addSubview:pwdLb];
    
    UILabel *newPwdLb=[[UILabel alloc]initWithFrame:CGRectMake(10, 135, 80, 25)];
    newPwdLb.text = @"新设密码";
    newPwdLb.font=[UIFont systemFontOfSize:13];
    [self.view addSubview:newPwdLb];
    
    UILabel *inputPwdLb=[[UILabel alloc]initWithFrame:CGRectMake(10, 165, 190, 25)];
    inputPwdLb.text = @"再次输入";
    inputPwdLb.font=[UIFont systemFontOfSize:13];
    [self.view addSubview:inputPwdLb];
    
    pwdTf = [[UITextField alloc] initWithFrame:CGRectMake(75, 105, WIN_WIDTH-85, 25)];
    [pwdTf setSecureTextEntry:YES];
    pwdTf.font=[UIFont systemFontOfSize:14];
    pwdTf.placeholder = @"";
    UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(0, 24, WIN_WIDTH-85, 1)];
    line1.backgroundColor = TEXT_COLOR;
    [pwdTf addSubview:line1];
    [self.view addSubview:pwdTf];
    
    newPwdTf = [[UITextField alloc] initWithFrame:CGRectMake(75, 135, WIN_WIDTH-85, 25)];
     [newPwdTf setSecureTextEntry:YES];
    newPwdTf.font=[UIFont systemFontOfSize:14];
    newPwdTf.placeholder = @"";
    UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(0, 24, WIN_WIDTH-85, 1)];
    line2.backgroundColor = TEXT_COLOR;
    [newPwdTf addSubview:line2];
    [self.view addSubview:newPwdTf];
    
    inputPwdTf = [[UITextField alloc] initWithFrame:CGRectMake(75, 165, WIN_WIDTH-85, 25)];
    [inputPwdTf setSecureTextEntry:YES];
    inputPwdTf.font=[UIFont systemFontOfSize:14];
    inputPwdTf.placeholder = @"";
    UIView *line3 = [[UIView alloc] initWithFrame:CGRectMake(0, 24, WIN_WIDTH-85, 1)];
    line3.backgroundColor = TEXT_COLOR;
    [inputPwdTf addSubview:line3];
    [self.view addSubview:inputPwdTf];
    
    UIButton *sureBtn = [[UIButton alloc] initWithFrame: CGRectMake(10, 215, WIN_WIDTH-20, 40)];
    [sureBtn setTitle:@"确  定" forState:0];
    sureBtn.backgroundColor = TITLE_COLOR;
    [self.view addSubview:sureBtn];
    sureBtn.clipsToBounds = YES;
    sureBtn.layer.cornerRadius = 1.0f;
    [sureBtn addTarget:self action:@selector(sureBtnClick)forControlEvents:UIControlEventTouchUpInside];
}

-(void)sureBtnClick{

    if ([self checkEdit]) {
        if ([self checkPwd]) {
            [self postPwd];
        }else{
            [[[ZFToast alloc] init] popUpToastWithMessage:@"两次输入不一致"];
        }
    } else {
    }
   
}

-(BOOL)checkPwd{
    if ([newPwdTf.text isEqual:inputPwdTf.text]) {
        return YES;
    } else{
        return NO;
    }
}

-(BOOL)checkEdit{
    
    if (([pwdTf.text length] == 0)||([newPwdTf.text length] == 0)||([inputPwdTf.text length] == 0)) {
        [[[ZFToast alloc] init] popUpToastWithMessage:@"密码不能为空"];
        return NO;
    }else{
        return YES;
    }
}

-(void) postPwd{
    
     NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
     [params setValue:pwdTf.text forKey:@"passWord"];
     [params setValue:[UserInfo setData].userName forKey:@"userName"];
    [[NetworkTool sharedTool] requestWithURLString:Api(modifyPwd) parameters:params method:@"POST" callBack:^(id responseObject) {
        NSLog(@"%@",responseObject);
        if([[responseObject objectForKey:@"state"] isEqual:@"success"]){
            [[[ZFToast alloc] init] popUpToastWithMessage:@"修改成功"];
            SetVC *personVC = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
            [self.navigationController popToViewController:personVC animated:true];
            self.tabBarController.tabBar.hidden = NO;
        }else {
            [[[ZFToast alloc] init] popUpToastWithMessage:@"网络错误！"];
        }
    }];
}

@end
