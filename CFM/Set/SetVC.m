//
//  MeVC.m
//  HSE
//
//  Created by Selab on 4/15/17.
//  Copyright © 2017 Selab. All rights reserved.
//

#import "SetVC.h"
#import "UpdatePwdVC.h"
//#import "ScanViewController.h"
#import "AboutUsVC.h"
//#import "DeviceBindVC.h"
#import "ContactVC.h"

@interface SetVC ()<UIAlertViewDelegate>

@end

UIView *v1;

@implementation SetVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = BG_COLOR;
    self.navigationItem.title = @"设置";
    self.tabBarController.tabBar.hidden = NO;
    self.view.hidden = NO;
    self.backBtn.hidden = YES;
    [self.navigationItem setHidesBackButton:YES];
    [self initUI];
}

#pragma mark InitView
- (void)initUI{
    
    
    UIView *v4 = [self setView:@"check" title:@"设备关联" top:61+30];
    UITapGestureRecognizer *vgr4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bindClick)];
    [v4 addGestureRecognizer:vgr4];
    //[self.view addSubview:v4];
    
    v1 = [self setView:@"pwdupdate" title:@"修改密码" top:61+30+41];
    UITapGestureRecognizer *vgr1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pwdClick)];
    [v1 addGestureRecognizer:vgr1];
    [self.view addSubview:v1];
    
    UIView *v2 = [self setView:@"check" title:@"关于我们" top:61+60+82];
    UITapGestureRecognizer *vgr2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(aboutClick)];
    [v2 addGestureRecognizer:vgr2];
    [self.view addSubview:v2];
    
    UIView *v5 = [self setView:@"check" title:@"联系方式" top:61+60+123];
    UITapGestureRecognizer *vgr5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(modifyClick)];
    [v5 addGestureRecognizer:vgr5];
    [self.view addSubview:v5];
    
    
    UIView *v3=[[UIView alloc]initWithFrame:CGRectMake(0, 61+30+282, WIN_WIDTH, 50)];
    v3.backgroundColor=[UIColor whiteColor];
    v3.layer.borderWidth = 1;
    v3.layer.borderColor = (__bridge CGColorRef _Nullable)(BG_COLOR);
    
    UILabel *nameLb = [[UILabel alloc]initWithFrame:
                       CGRectMake((WIN_WIDTH-120)/2, 15, 120, 20)];
    nameLb.text = @"退出应用";
    nameLb.textAlignment = UITextAlignmentCenter;
    nameLb.textColor = TEXT_COLOR;
    nameLb.font=FONT(14);
    [v3 addSubview:nameLb];
    UITapGestureRecognizer *vgr3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(exitClick)];
    [v3 addGestureRecognizer:vgr3];
    [self.view addSubview:v3];
    
    
}

- (UIView*)setView:(NSString *)imageName title:(NSString *)title top:(int)top{
    
    UIView *view = [[UIView alloc] initWithFrame:
                    CGRectMake(0, top, WIN_WIDTH, 41)];
    view.backgroundColor = [UIColor whiteColor];
    view.userInteractionEnabled = YES;
    UIImageView *leftImg = [[UIImageView alloc] initWithFrame:
                            CGRectMake(10, 5, 30, 30)];
    leftImg.image = [UIImage imageNamed:@""];
    
    UIImageView *rightImg = [[UIImageView alloc] initWithFrame:
                             CGRectMake(WIN_WIDTH-30, 10, 15, 20)];
    rightImg.image = [UIImage imageNamed:@"next_step"];
    [view addSubview:rightImg];
    
    UILabel *nameLb = [[UILabel alloc]initWithFrame:
                       CGRectMake(30, 10, 100, 20)];
    nameLb.text = title;
    nameLb.textColor = TEXT_COLOR;
    nameLb.font=FONT(13);
    [view addSubview:nameLb];
    
    UIView *line = [[UIView alloc] initWithFrame:
                    CGRectMake(5, 40, WIN_WIDTH-10, 1)];
    line.backgroundColor = BG_COLOR;
    [view addSubview:line];
    return view;
}

-(void)bindClick{
    
//    DeviceBindVC *vc = [[DeviceBindVC alloc] init];
//    [self.navigationController pushViewController:vc animated:YES];
}

-(void)pwdClick{
    
    UpdatePwdVC *vc = [[UpdatePwdVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)aboutClick{
    
    AboutUsVC *vc = [[AboutUsVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)modifyClick{
    
    ContactVC *vc = [[ContactVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)exitClick{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"退出应用" message:@"是否退出？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex) {
        exit(0);
        EMError *error = [[EMClient sharedClient] logout:YES];
        if (!error) {
            NSLog(@"退出成功");
        }
    } else {
    }
}

- (void)viewDidAppear:(BOOL)animated{
    //   NSLog(@"返回执：%d",animated);
    //    if(animated){
    //        self.tabBarController.tabBar.hidden = NO;
    //        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"扫描结果" message:self.coder preferredStyle:UIAlertControllerStyleAlert];
    //        [alert addAction:[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    //
    //        }]];
    //        [self presentViewController:alert animated:YES completion:nil];
    //    }
    NSLog(@"返回执行！！");
}

@end

