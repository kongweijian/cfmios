//
//  WarnHistoryVC.h
//  CFM
//
//  Created by Selab on 04/02/2018.
//  Copyright © 2018 Selab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WarnHistoryVC : BaseVC

@property (nonatomic,retain) UITableView *tableView;
@property (nonatomic,retain) NSMutableArray *arr;
@property(nonatomic)NSInteger childId;

@end
