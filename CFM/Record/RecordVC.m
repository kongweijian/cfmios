//
//  ViewController.m
//  miSearch
//
//  Created by miicaa_ios on 16/8/3.
//  Copyright (c) 2016年 xuxuezheng. All rights reserved.
//
#import "RecordVC.h"
#import "RecordCell.h"
#import "WarnHistoryVC.h"

@interface RecordVC ()<UITableViewDataSource,UITableViewDelegate,UITabBarDelegate>
@end

@implementation RecordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"记录";
    self.backBtn.hidden = YES;
    [self.navigationItem setHidesBackButton:YES];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,WIN_WIDTH, WIN_HEIGHT-80) style:UITableViewStylePlain];
    _tableView.delegate  =self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [[UIView alloc] init];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UILongPressGestureRecognizer *longGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGesture:)];
    [self.tableView addGestureRecognizer:longGesture];
    [self.view addSubview:_tableView];
    _arr = [[NSMutableArray alloc] init];
  
    [self getRecordList];
    
}

- (UIImage *)createImageWithColor: (UIColor *) color
{
    CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   RecordCell*cell = [tableView dequeueReusableCellWithIdentifier:@"SearchCell"];
    if (!cell) {
        cell = [[RecordCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SearchCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (_arr.count>0) {
        NSMutableDictionary *temp = _arr[indexPath.row];
        cell.name.text = [NSString stringWithFormat:@"%@%@", @"姓名：",[temp objectForKey:@"childName"]];
        cell.bed.text = [NSString stringWithFormat:@"%@%@", @"床位：",[temp objectForKey:@"bed"]];
        NSString *createTime = [temp objectForKey:@"createTime"];
        NSString *leaveTime = [temp objectForKey:@"leaveTime"];
        if([leaveTime isEqual:[NSNull null]]){
             cell.time.text = [NSString stringWithFormat:@"%@%@", @"使用时间：",createTime];
        }else{
            cell.time.text = [NSString stringWithFormat:@"%@%@%@%@", @"使用时间：",createTime,@"至 ",leaveTime];
        }
    } else {
        NSLog(@"searchText --%@",@"空");
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableDictionary *temp = _arr[indexPath.row];
    WarnHistoryVC *vc = [[WarnHistoryVC alloc] init];
    vc.childId = [[temp objectForKey:@"childId"] integerValue];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void) getRecordList{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:@([UserInfo setData].userId) forKey:@"userId"];
    [[NetworkTool sharedTool] requestWithURLString:Api(recordList) parameters:params method:@"GET" callBack:^(id responseObject) {
        NSLog(@"%@",responseObject);
        _arr = [responseObject objectForKey:@"childRecordList"];
        [self.tableView reloadData];
    }];
}

- (void)longPressGesture:(UILongPressGestureRecognizer *)longGesture
{
    CGPoint location = [longGesture locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    _index=indexPath.row;
    if (longGesture.state == UIGestureRecognizerStateEnded){
        return;
    }else if (longGesture.state == UIGestureRecognizerStateBegan){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"是否确定不再关注改病人？若不再关注则收不到他的报警信息" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alert show];
        
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex) {
        NSMutableDictionary *temp = _arr[_index];
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setValue:@([[temp objectForKey:@"childId"] integerValue]) forKey:@"childId"];
        [[NetworkTool sharedTool] requestWithURLString:Api(leaveChild) parameters:params method:@"GET" callBack:^(id responseObject) {
            NSLog(@"%@",responseObject);
            [self getRecordList];
            [self.tableView reloadData];
        }];
    } else {
    }
}


- (void)viewDidAppear:(BOOL)animated{
    
    self.tabBarController.tabBar.hidden = NO;
    [self getRecordList];
    [super viewDidAppear:animated];
}

//视图将要消失
- (void)viewWillDisappear:(BOOL)animated {
    NSLog(@"%s", __FUNCTION__);
    [super viewWillDisappear:animated];
}

//视图已经消失
- (void)viewDidDisappear:(BOOL)animated {
    [self getRecordList];
    NSLog(@"%s", __FUNCTION__);
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end




