//
//  RecordVC.h
//  CFM
//
//  Created by Selab on 03/02/2018.
//  Copyright © 2018 Selab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordVC : BaseVC

@property (nonatomic,retain) UITableView *tableView;
@property (nonatomic,retain) NSMutableArray *arr;
@property (nonatomic) NSInteger index;
@property (nonatomic,copy) NSDictionary *userInfo;

@end
