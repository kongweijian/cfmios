//
//  WarnHistoryCell.h
//  CFM
//
//  Created by Selab on 04/02/2018.
//  Copyright © 2018 Selab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WarnHistoryCell : UITableViewCell

@property (strong,nonatomic) UIImageView *img;
@property (strong,nonatomic) UILabel *name;
@property (strong,nonatomic) UILabel *time;
@property (strong,nonatomic) UIView *line;

@end
