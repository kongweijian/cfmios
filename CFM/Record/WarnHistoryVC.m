//
//  WarnHistoryVC.m
//  CFM
//
//  Created by Selab on 04/02/2018.
//  Copyright © 2018 Selab. All rights reserved.
//

#import "WarnHistoryVC.h"
#import "WarnHistoryCell.h"

@interface WarnHistoryVC ()<UITableViewDataSource,UITableViewDelegate,UITabBarDelegate>
@end

@implementation WarnHistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"报警历史";
    [self.navigationItem setHidesBackButton:YES];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,WIN_WIDTH, WIN_HEIGHT-80) style:UITableViewStylePlain];
    _tableView.delegate  =self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [[UIView alloc] init];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    _arr = [[NSMutableArray alloc] init];
    
    [self getWarnHistoryList];
    
}

- (UIImage *)createImageWithColor: (UIColor *) color
{
    CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WarnHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchCell"];
    if (!cell) {
        cell = [[WarnHistoryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SearchCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (_arr.count>0) {
        NSMutableDictionary *temp = _arr[indexPath.row];
        cell.time.text = [NSString stringWithFormat:@"%@%@", @"时间：",[temp objectForKey:@"dateTime"]];
        NSString *state = [temp objectForKey:@"deviceState"];
        if ([state containsString:@"正常"]) {
             UIImage *image = [UIImage imageNamed:@"normal"];
             cell.img.image = image;
        }else if ([state containsString:@"脱落"]) {
             UIImage *image = [UIImage imageNamed:@"yichangs"];
             cell.img.image = image;
        }else{
             UIImage *image = [UIImage imageNamed:@"duan"];
             cell.img.image = image;
        }
    } else {
        NSLog(@"searchText --%@",@"空");
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(void) getWarnHistoryList{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:@(_childId) forKey:@"childId"];
    [[NetworkTool sharedTool] requestWithURLString:Api(warnHistory) parameters:params method:@"GET" callBack:^(id responseObject) {
        NSLog(@"%@",responseObject);
        _arr = [responseObject objectForKey:@"warnHistoryList"];
        [self.tableView reloadData];
    }];
}


@end
