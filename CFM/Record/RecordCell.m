//
//  RecordCell.m
//  CFM
//
//  Created by Selab on 02/02/2018.
//  Copyright © 2018 Selab. All rights reserved.
//

#import "RecordCell.h"

@implementation RecordCell
@synthesize name,bed,time,img,line;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIImage *image = [UIImage imageNamed:@"baby"];
        img=[[UIImageView alloc]initWithFrame:CGRectMake(20, 10, 60, 60)];
        img.layer.masksToBounds = YES;
        img.layer.cornerRadius = 22;
        img.image=image;
        [self.contentView addSubview:img];
        
        name=[[UILabel alloc]initWithFrame:CGRectMake(96, 15, 120, 17)];
        name.backgroundColor=[UIColor clearColor];
        name.font=[UIFont systemFontOfSize:16];
        name.textColor=TEXT_COLOR;
        [self.contentView addSubview:name];
        
        bed=[[UILabel alloc]initWithFrame:CGRectMake(220, 15, 120, 17)];
        bed.backgroundColor=[UIColor clearColor];
        bed.font=[UIFont systemFontOfSize:16];
        bed.textColor=TEXT_COLOR;
        bed.text=@"";
        [self.contentView addSubview:bed];
        
        time=[[UILabel alloc]initWithFrame:CGRectMake(96, 48, 324, 17)];
        time.textColor=TEXT_COLOR;
        time.backgroundColor=[UIColor clearColor];
        time.font=[UIFont systemFontOfSize:14];
        [self.contentView addSubview:time];
        
        line=[[UIView alloc]initWithFrame:CGRectMake(0, 79, WIN_WIDTH, 0.5)];
        line.backgroundColor=[UIColor grayColor];
        //.layer.borderColor =[ [UIColor grayColor] CGColor];
        
        [self.contentView addSubview:line];
        
        
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
