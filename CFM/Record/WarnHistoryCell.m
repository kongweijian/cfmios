//
//  WarnHistoryCell.m
//  CFM
//
//  Created by Selab on 03/02/2018.
//  Copyright © 2018 Selab. All rights reserved.
//

#import "WarnHistoryCell.h"

@implementation WarnHistoryCell

@synthesize name,time,img,line;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIImage *image = [UIImage imageNamed:@"icon"];
        img=[[UIImageView alloc]initWithFrame:CGRectMake(20, 10, 60, 60)];
        img.layer.masksToBounds = YES;
        img.layer.cornerRadius = 22;
        img.image=image;
        [self.contentView addSubview:img];
        
        time=[[UILabel alloc]initWithFrame:CGRectMake(100, 30, 240, 20)];
        time.textColor=TEXT_COLOR;
        time.backgroundColor=[UIColor clearColor];
        time.font=[UIFont systemFontOfSize:14];
        time.text=@"脱落时间：23:12:21";
        [self.contentView addSubview:time];
        
        line=[[UIView alloc]initWithFrame:CGRectMake(0, 79, WIN_WIDTH, 0.5)];
        line.backgroundColor=[UIColor grayColor];
        //.layer.borderColor =[ [UIColor grayColor] CGColor];
        
        [self.contentView addSubview:line];
        
        
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end


